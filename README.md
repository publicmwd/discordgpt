# GPT-3 Discord Bot
This is a Discord bot that uses the GPT-3 API provided by OpenAI to generate natural language and code responses to user requests.

## Requirements

- A Discord account and a server where you can invite the bot to 
- An OpenAI account and an API key for the GPT-3 model
- Python 3.6 or higher
- The following Python libraries: discord.py, openai

## Installation

- Clone this repository
- Create a virtual environment and activate it
- Install the necessary dependencies by running pip install -r requirements.txt
- Create a new application and a bot account on the Discord Developer Portal
- Get the bot's token and add it to the systemd entry
- Create an account on OpenAI and get an API key for the GPT-3 model
- Add the API key to the systemd entry
- Prepare install.bash with correct paths
- Prepare gptbot.service with correct paths
- Run install.bash

## Usage
The bot will respond to the !q command followed by a request for natural language responses.
The bot will respond to the !qc command followed by a programming language and a request for code responses.

### Example

User: "!q What is the weather like today?"

Bot: "I'm sorry, I am not able to access the current weather. However, generally speaking, the weather today is expected to be sunny with a high of 75 degrees."

User: "!qc split a string in python"

Bot: "mystring.split()"

## Deployment
You can deploy the bot on a hosting platform such as Heroku or DigitalOcean to run it 24/7.

## Note
This is a simple example and you may want to add some error handling and more functionality to the bot.

## Acknowledgments
OpenAI for providing the GPT-3 API

Discord.py for providing a simple way to interact with the Discord API

Mijnwebsitedesign.nl for writing the initial script