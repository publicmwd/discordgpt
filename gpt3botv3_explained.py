import openai
import discord
import shlex
import logging
import re
from discord.ext import commands
import os
from typing import Optional

# Get the Discord bot token from environment variable
TOKEN = os.environ.get('DISCORD_TOKEN', None)
if TOKEN is None:
    raise ValueError("Please set the DISCORD_TOKEN environment variable.")

# Get the OpenAI API key from environment variable
openai_api_key = os.environ.get('OPENAI_API_KEY', None)
if openai_api_key is None:
    raise ValueError("Please set the OPENAI_API_KEY environment variable.")
openai.api_key = openai_api_key

#initialize the logging module to log errors in bot.log
logging.basicConfig(filename='bot.log', level=logging.ERROR)

class GPT3Bot(commands.Bot):
    """
    A subclass of commands.Bot that uses OpenAI's GPT-3 API to generate
    text and code responses
    """
    def __init__(self, command_prefix, intents):
        """
        Initialize the GPT3Bot class
        """
        super().__init__(command_prefix, intents=intents)
    async def on_message(self, message):
        """
        This function is called whenever a message
        is received. It checks if the message starts with !, !q, or !qc,
        and responds accordingly.
        """
        if message.author == self.user:
            return
        if not message.content.startswith("!"):
            return
        try:
            if message.content.startswith('!q'):
                # !q command is used to generate text responses
                query = message.content[3:]
                temperature, max_tokens = parse_prompt_args(shlex.split(query))
                response = openai.Completion.create(
                    engine="text-davinci-002",
                    prompt=f"{query}",
                    temperature=temperature,
                    max_tokens=max_tokens,
                    n=1,
                    stop=None
                )
                await message.channel.send(f'{message.author.mention} {response["choices"][0]["text"]}')
            elif message.content.startswith('!qc'):
                # !qc command is used to generate code responses
                query = message.content[4:]
                temperature, max_tokens = parse_prompt_args(shlex.split(query))
                response = openai.Completion.create(
                    engine="text-code-001",
                    prompt=f"{query}",
                    temperature=temperature,
                    max_tokens=max_tokens,
                    n=1,
                    stop=None
                )
                if re.search(r"<code>", response["choices"][0]["text"]):
                    response_text = re.sub("<code>", "```", response["choices"][0]["text"])
                    response_text = re.sub("</code>", "```", response_text)
                    await message.channel.send(f'{message.author.mention} {response_text}')
                else:
                    await message.channel.send(f'{message.author.mention} {response["choices"][0]["text"]}')
            elif message.content.startswith("!h"):
                # !h command is used to display usage and options
                usage = "Usage: !q [prompt] [-t temperature] [-m max_tokens]"
                options = "Options: \n -t temperature (default: 0.5)\n -m max_tokens (default: 2048)"
                await message.channel.send(f'{message.author.mention} {usage}')
                await message.channel.send(options)
            else:
                await message.channel.send("Command not recognized. Please use !q or !qc or !h.")
        except ValueError as e:
            # Log error in bot.log
            logging.error(f"An error occurred: {e}")
            await message.channel.send(f"{message.author.mention} Sorry, an error occurred while parsing the command. Please check the command format and try again.")

def parse_prompt_args(args: list) -> tuple:
    """
    Parses the command arguments for temperature and max_tokens
    """
    temperature = 0.5
    max_tokens = 2048
    for arg in args:
        if arg.startswith("-t"):
            temperature = float(arg.split("=")[1])
            if temperature < 0 or temperature > 1:
                raise ValueError("Temperature should be between 0 and 1.")
        elif arg.startswith("-m"):
            max_tokens = int(arg.split("=")[1])
    return (temperature, max_tokens)

if __name__ == "__main__":
    # Initialize the bot with command prefix '!' and intents
    bot = GPT3Bot('!', intents=discord.Intents.all())
    bot.run(TOKEN)
