#!/bin/bash

# Install the required python packages
pip3 install -r /path/to/your/bot/requirements.txt

# Copy the systemd service file to the appropriate location
sudo cp /path/to/your/bot/gpt3bot.service /etc/systemd/system/

# Reload the systemd daemon to pick up the new service
sudo systemctl daemon-reload

# Enable the service to start on boot
sudo systemctl enable gptbot

# Start the service
sudo systemctl start gpt3bot
